mod http_helper;
mod api;

use http_helper::request::get_file;
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;
use std::thread;
use clap::{Arg, App};
use rand::{Rng, thread_rng};

fn download_file(image: api::Image) 
    -> Result<(), Box<dyn Error>> {
    
    let filename: String = format!("{}/.acapi_database/{}", 
                                   std::env::var("HOME")?,
                                   image.file_name);
    let mut file = File::create(filename)?;
    let image_file = get_file(image.image_url)?;
    file.write(&image_file)?;
    Ok(())
}

fn download_page(page_id: i32, method: &String, download_method: &String) -> Result<(), Box<dyn Error>>{
    let client = api::Client::new();
    println!("downloading from '{}'", &method);
    println!("current page is {}.", page_id);
    let mut image_counter = 1;
    let mut threads = Vec::new();
    let page = client.get_page(&method, page_id)?;
    let mut total_bytes = 0;
    for image in page.images {
        total_bytes += image.file_size.parse::<i32>().unwrap();
        let thread = thread::spawn(move || {
            println!("{}/30 ({}) page {}; metadata: {}x{}, {}Kb", 
                     image_counter, image.file_name, page_id, 
                     image.width, image.height, 
                     ((image.file_size.parse::<i32>().unwrap()) / 1024));
            match download_file(image){
                Err(why) => println!("ERROR: {}/30. {}", image_counter, 
                                   why),
                Ok(()) => ()
            };

            println!("{}/30 done!", image_counter);
        });

        if download_method != "parallel" {
            match thread.join() {
                Err(why) => panic!(why),
                Ok(()) => ()
            }; 
        } else {
            threads.push(thread);
        }
        image_counter += 1;
    }

    println!("total to download: {}Kb", total_bytes / 1024);

    for thread in threads {
        match thread.join(){
            Err(why) => panic!(why),
            Ok(()) => ()
        };
    }
    Ok(())
}

fn set_as_wallpaper(filename: &String) -> Result<(), Box<dyn Error>> {
    wallpaper::set_from_path(filename)?;
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("acwp")
        .version("0.1.0")
        .author("Whitantire <jarikus111222333@gmail.com>")
        .about("Alphacoders wallpapers parser.")
        .arg(Arg::with_name("method_name")
             .short('m')
             .long("method_name")
             .takes_value(true)
             .about("you can specify download method. you can use: by_favorites, newest"))
        .arg(Arg::with_name("page")
             .short('p')
             .long("page")
             .takes_value(true)
             .about("specify the page."))
        .arg(Arg::with_name("download_method")
             .short('d')
             .long("download_method")
             .takes_value(true)
             .about("download method. you can use: parallel, consistenly"))
        .arg(Arg::with_name("page_range")
             .short('r')
             .long("page_range")
             .takes_value(true)
             .about("specify page range to download. syntax: from:to; Ex: 1:10"))
        .arg(Arg::with_name("set_random_wallpaper")
             .short('R')
             .long("set_random_wallpaper")
             .takes_value(false)
             .about("set random wallpaper from downloaded walls.")).get_matches();

    let db_folder: &String = &format!("{}/.acapi_database", std::env::var("HOME")?);

    if !std::path::Path::new(db_folder).exists() {
        std::fs::create_dir(db_folder)?;
    }

    let srw = matches.is_present("set_random_wallpaper");
    if srw {
        let files = std::fs::read_dir(db_folder)?
            .collect::<Result<Vec<_>, _>>()?;
        if files.len() != 0 {
            let mut rnd_thread = thread_rng();
            let file_id = rnd_thread.gen_range(0, files.len());
            let filename: String = match files[file_id]
                .file_name().into_string() {
                    Err(why) => panic!("123 {:?}", why),
                    Ok(filename) => filename
                };
            let filename: String = format!("{}/{}", db_folder, filename);
            println!("{} files founded in database; file_id: {}; filename: {}", 
                     files.len(), &file_id, &filename);
            set_as_wallpaper(&filename)?;
        }
        std::process::exit(0);
    }

    let method_name = matches.value_of("method_name")
        .unwrap_or("by_favorites").to_string();
    let page: i32 = matches.value_of("page").unwrap_or("1").parse().unwrap();
    let download_method = matches.value_of("download_method")
        .unwrap_or("parallel").to_string();
    let page_range = matches.value_of("page_range").unwrap_or("none");

    println!("method_name: {}. page: {}. download_method: {}, page_range: {}",
             method_name, page, download_method, page_range);
    
    if page_range != "none" {
        let split = page_range.split(":").collect::<Vec<&str>>();
        let from: i32 = split[0].parse().unwrap();
        let to: i32 = split[1].parse().unwrap();
        for index in from..=to {
            download_page(index, &method_name, &download_method)?;
        }
        std::process::exit(0);
    }
        download_page(page, &method_name, &download_method)?;

    Ok(())
}
