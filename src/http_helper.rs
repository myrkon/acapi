#[allow(dead_code)]
pub mod request {
    pub fn get(url: String) -> Result<String, Box<dyn std::error::Error>> {
        let response = reqwest::blocking::get(&url)?;
        // let json = response.json::<HashMap<String, String>>()?;
        let json = response.text().unwrap();
        Ok(json)
    }

    pub fn get_file(url: String) -> Result<bytes::Bytes, Box<dyn std::error::Error>> {
        let client = reqwest::blocking::Client::builder()
            .timeout(None).build()?;
        let response = client.get(&url).send()?;
        // let json = response.json::<HashMap<String, String>>()?;
        let bytes = response.bytes()?;
        Ok(bytes)
    }

    pub fn post(url: String, body: String) 
        -> Result<String, Box<dyn std::error::Error>> {
        let client = reqwest::blocking::Client::new();
        let res: reqwest::blocking::Response = 
            client.post(&url).body(body).send()?;
        let text: String = res.text().unwrap();
        Ok(text)
    }

}

pub fn str2obj(str_obj: &String) -> Result<json::JsonValue, 
   Box<dyn std::error::Error>> {
    let obj = json::parse(str_obj).unwrap();
    Ok(obj)
}
