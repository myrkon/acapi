use crate::http_helper::{request::get, str2obj};
use std::error::Error;
use std::collections::HashMap;

pub struct Page {
    pub id: i32,
    pub page_len: i32,
    pub images: Vec<Image>
}

pub struct Image {
    pub id: String,
    pub width: String,
    pub height: String,
    pub file_name: String,
    pub file_type: String,
    pub file_size: String,
    pub image_url: String,
    pub thumb_url: String
}

pub struct Client {
    access_key: String
}

impl Client {
    pub fn new() -> Self {
        Client {
            access_key: "82fb7dc5c1dc0e67558a8c5b134704e3".to_string()
        }
    }

    pub fn get_by(&self, method: &String, page: i32) -> Result<String, Box<dyn Error>> {
        let response = get(format!(
                "https://wall.alphacoders.com/api2.0/get.php?auth={}&method={}&page={}", 
                self.access_key, 
                method,
                page))?;
        Ok(response)
    }

    pub fn get_page(&self, method: &String, page: i32) -> Result<Page, Box<dyn Error>> {
        let response = str2obj(&self.get_by(method, page)?)?;
        let mut image_urls = HashMap::new();
        let mut thumbs_urls = HashMap::new();

        for index in 0..response["wallpapers"].len() {
            let image_url: String = 
                response["wallpapers"][index]["url_image"].to_string();
            let thumb_url: String = 
                response["wallpapers"][index]["url_thumb"].to_string();
            image_urls.insert(index, image_url); 
            thumbs_urls.insert(index, thumb_url);
        }

        let mut images = Vec::new();
        let len = image_urls.len();
        for index in 0..len {
            let idx = &(index as usize);
            let wall = &response["wallpapers"][index];
            let id = String::from(wall["id"].as_str().unwrap());
            let width = String::from(wall["width"].as_str().unwrap());
            let height = String::from(wall["height"].as_str().unwrap());
            let file_type = String::from(
                    wall["file_type"].as_str().unwrap());
            let file_size = String::from(
                wall["file_size"].as_str().unwrap());
            let image_url = String::from(&image_urls[idx]);
            let thumb_url = String::from(&thumbs_urls[idx]);

            let split = image_url.split("/");
            let split = split.collect::<Vec<&str>>();
            let file_name = split[split.len()-1].to_string();

            images.push(Image {
                id: id,
                width: width,
                height: height, 
                file_name: file_name,
                file_type: file_type,
                file_size: file_size,
                image_url: image_url,
                thumb_url: thumb_url 
            });
        }

        Ok(Page {
            id: page,
            page_len: image_urls.len() as i32,
            images: images
        })
    }
}
